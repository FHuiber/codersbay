package fahrzeugArten;

import java.util.Calendar;

public class Lkw extends Fahrzeug {

    private final double rabattProJahr = 6d;

    public Lkw(int id, int baujahr, String marke, double grundpreis){
        super(id, baujahr, marke, grundpreis);
    }

    @Override
    public double getRabatt() {
        int aktuellesJahr = Calendar.getInstance().get(Calendar.YEAR);
        double rabatt = (aktuellesJahr - super.getBaujahr()) * rabattProJahr;
        if (rabatt > 15) {
            rabatt = 15;
        }
        return rabatt;
    }

    @Override
    public void print() {
        System.out.print("LKW:\nID: " + super.getId() + "\nBaujahr: " + super.getBaujahr() + "\nMarke: " + super.getMarke() + "\nGrundpreis: "
                + super.getPreis() + "€\nRabatt: " + getRabatt() + "%\nPreis: " + getPreis() + "€\nLKW PRINT ENDE\n");
    }

}
// LKW Rabatt pro Jahr 6% min 0% max 15% grundpreis