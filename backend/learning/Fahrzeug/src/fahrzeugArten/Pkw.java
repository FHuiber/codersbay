package fahrzeugArten;

import java.util.Calendar;

public class Pkw extends Fahrzeug{

    private int vorfuehrwagenJahr;
    private final double rabattProJahr = 5d, rabattProVorfuehrjahr = 3d;
 
    public Pkw(int id, int baujahr, String marke, double grundpreis, int vorfuehrwagenJahr) {
        super(id, baujahr, marke, grundpreis);
        this.vorfuehrwagenJahr = vorfuehrwagenJahr;
    }

    @Override
    public double getRabatt() {
        int aktuellesJahr = Calendar.getInstance().get(Calendar.YEAR);
        double rabatt = ((aktuellesJahr - super.getBaujahr()) * rabattProJahr) + ((vorfuehrwagenJahr - super.getBaujahr()) * rabattProVorfuehrjahr);
        if (rabatt > 10) {
            rabatt = 10;
        }else if(rabatt < 0){
            rabatt = 0;
        }
        return rabatt;
    }

    public boolean setVorfuehrwagenJahr(int jahr) {
        int aktuellesJahr = Calendar.getInstance().get(Calendar.YEAR);
        if (jahr < super.getBaujahr() || jahr > aktuellesJahr) {
            System.out.println("Ungültig");
            return false;
        } else {
            this.vorfuehrwagenJahr = jahr;
            System.out.println("Richtig");
            return true;
        }
    }

    public int getVorfuehrwagenJahr() {
        return vorfuehrwagenJahr;
    }

    @Override
    public void print() {
        System.out.print("PKW:\nID: " + super.getId() + "\nBaujahr: " + super.getBaujahr() + "\nMarke: " + super.getMarke() + "\nGrundpreis: "
                + super.getGrundpreis() + "€\nRabatt: " + getRabatt() + "%\nPreis: " + getPreis() + "€\nPKW PRINT ENDE\n");

    }

}
// Rabatt pro jahr 5% + 3% pro Vorführwagen min 0% max 10% grundpreis