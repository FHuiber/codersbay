package fahrzeugArten;

public abstract class Fahrzeug {
    private int id, baujahr;
    private String marke;
    private double grundpreis;
    
    public Fahrzeug(int id, int baujahr, String marke, double grundpreis) {
        this.id = id;
        this.baujahr = baujahr;
        this.marke = marke;
        this.grundpreis = grundpreis;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBaujahr() {
        return baujahr;
    }

    public void setBaujahr(int baujahr) {
        this.baujahr = baujahr;
    }

    public String getMarke() {
        return marke;
    }

    public void setMarke(String marke) {
        this.marke = marke;
    }

    public double getGrundpreis() {
        return grundpreis;
    }

    public void setGrundpreis(double grundpreis) {
        this.grundpreis = grundpreis;
    }

    public double getPreis() {
        double preis = grundpreis - (grundpreis / 100 * getRabatt());
        return preis;
    }

    @Override
    public String toString() {
        return "ID:" + id + " Marke: " + marke + " Baujahr: " + baujahr + " Grundpreis:" + grundpreis + " Rabatt: " + getRabatt() + " Preis: " + getPreis() + "\n";
    }

    public abstract double getRabatt();
    //public abstract double getPreis();
    public abstract void print();
    
}
