package infrastruktur;

import java.util.ArrayList;
import java.util.List;

import fahrzeugArten.Fahrzeug;
import fahrzeugArten.Lkw;
import fahrzeugArten.Pkw;

public class Autohaus {
   private final int mitarbeiter,  abstellplaetze;
   private final String name;

   List<Fahrzeug> fahrzeuge = new ArrayList<Fahrzeug>();

   public Autohaus(int mitarbeiter, int abstellplaetze, String name) {
    this.mitarbeiter = mitarbeiter;
    this.abstellplaetze = abstellplaetze;
    this.name = name;
}

public int getMitarbeiter() {
    return mitarbeiter;
}

public int getAbstellplaetze() {
    return abstellplaetze;
}

public String getName() {
    return name;
}

public List<Fahrzeug> getFahrzeuge() {
    return fahrzeuge;
}

public void setFahrzeug (String fahrzeugTyp, int id, String marke, int baujahr, double grundpreis, int vorfuehrwagenJahr) {
   if (fahrzeugTyp.equals("PKW")) {
       //fahrzeuge.add(new Pkw(id, baujahr, vorfuehrwagenJahr, marke, grundpreis));
       Pkw newPkw = new Pkw(id, baujahr, marke, grundpreis, vorfuehrwagenJahr);
       fahrzeuge.add(newPkw);
   } else if (fahrzeugTyp.equals("LKW")) {
       Lkw newLkw = new Lkw(id, baujahr, marke, grundpreis);
       fahrzeuge.add(newLkw);
   } else {
       System.out.println("Fahrzeug ist weder LKW noch PKW");
   }
}

public boolean  zuVieleFahrzeuge(){
    if (fahrzeuge.size() > (mitarbeiter*3)) {
        System.out.println("Überlastet!!! Zu viele Fahrzeuge für unsere Mitarbeiter!!!");
        return true;
    }
    if (fahrzeuge.size() > this.abstellplaetze) {
        System.out.println("Überlastet!!! Zu viele Fahrzeuge für unsere Abstellplätze!!!");
        return true;
    }else{
        System.out.println("Alles Gut. Genug Abstellplätze und Mitarbeiter für die Fahrzeuge.");
        return false;
    }
    
   }

public void anzahlFahrzeuge(){
    System.out.println("Es gibt " + fahrzeuge.size() + " Fahrzeuge im Autohaus");
}

   //############################# Andere Methode ##################################
   /* 
    public boolean  zuVieleFahrzeuge(int fahrzeuge){
    return isUeberlastet(fahrzeuge, this.abstellplaetze, this.mitarbeiter);
   }

   public boolean  zuVieleFahrzeuge(int fahrzeuge, int abstellplaetze){
    return isUeberlastet(fahrzeuge, abstellplaetze, this.mitarbeiter);
   }

   private boolean isUeberlastet(int fahrzeuge, int abstellplaetze, int mitarbeiter){
    if (fahrzeuge > (mitarbeiter*3)) {
        System.out.println("Überlastet!!! Zu viele Fahrzeuge für unsere Mitarbeiter!!!");
        return true;
    }else{
        System.out.println("Alles Gut. Genug Mitarbeiter für die Fahrzeuge.");
    }
    if (fahrzeuge > abstellplaetze) {
        System.out.println("Überlastet!!! Zu viele Fahrzeuge für unsere Abstellplätze!!!");
        return true;
    }else{
        System.out.println("Alles Gut. Genug Abstellplätze für die Fahrzeuge.");
        return false;
    }
   }
   */

   @Override
public String toString() {
    return "Autohaus: " + name + "\nMitarbeiter: " + mitarbeiter  + "\nAbstellplaetze: " + abstellplaetze + "\nFahrzeuge:\n" + fahrzeuge;
}

public void print(){
       System.out.println(name);
   }
}
