package main;

import fahrzeugArten.Fahrzeug;
import infrastruktur.Autohaus;

public class Main {

    public static void main(String[] args) {

        System.out.println("TESTING");

        Autohaus autohaus = new Autohaus(2, 1, "Hauto Aus");
        autohaus.setFahrzeug("LKW", 1, "FIAT", 1990, 3000, -1);
        autohaus.setFahrzeug("PKW", 1, "BMW", 1990, 3000, 1995);

        System.out.println(autohaus);
        autohaus.anzahlFahrzeuge();
        for (Fahrzeug fahrzeug : autohaus.getFahrzeuge()) {
            fahrzeug.print();
        }
        
        autohaus.zuVieleFahrzeuge();

        System.out.println("TEST " + autohaus.getFahrzeuge().get(0).getRabatt());

}

}
