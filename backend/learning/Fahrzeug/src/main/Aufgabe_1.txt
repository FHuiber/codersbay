package main;

public class Main {
    public static void main(String[] args) {

        //String pkw1 = "Audi";
        String pkw1 = "Volvoo";
        String pkw2 = "Mercedes";
        String pkw3 = "VW";
        String lkw1 = "Volvo";
        String lkw2 = "VW";
        int lKWs = 2;
        int pKWs = 3;
        int anzahlFahrzeuge = pKWs + lKWs;
        double preis = 2001;
        double rabatt = 0.0;

        System.out.println(pkw1);
        System.out.println(pkw2);
        System.out.println(pkw3);
        System.out.println(lkw1);
        System.out.println(lkw2);
        System.out.println(lKWs);
        System.out.println(pKWs);
        System.out.println(anzahlFahrzeuge);
        System.out.println(preis);
        System.out.println(rabatt);

        for (int i = 1; i <= anzahlFahrzeuge; i++) {
            System.out.println("Es gibt ein neues Fahrzeug im Autohaus. Anzahl Fahrzeuge: " + i);
            if (i == anzahlFahrzeuge) {
                System.out.println("Folgende Marken sind zu verkaufen:\nPKW: " + pkw1 + " " + pkw2 + " " + pkw3
                        + "\nLKW: " + lkw1 + " " + lkw2);
            }
            while (i < anzahlFahrzeuge) {
                System.out.println("Es sind noch nicht alle Fahrzeuge registriert.");
                break;
            }

        }

        if (pkw1 == lkw1 || pkw1 == lkw2) {
            System.out.println("Es gibt einen PKW und LKW, die vom gleichen Hersteller sind.1");
        } else if (pkw2 == lkw1 || pkw2 == lkw2) {
            System.out.println("Es gibt einen PKW und LKW, die vom gleichen Hersteller sind.2");
        } else if (pkw3 == lkw1 || pkw3 == lkw2) {
            System.out.println("Es gibt einen PKW und LKW, die vom gleichen Hersteller sind.3");
        }

        anzahlFahrzeuge = 10;
        System.out.println("Es gibt neue Fahrzeuge im Autohaus. Anzahl Fahrzeuge: " + anzahlFahrzeuge);

        if (lkw1 != lkw2) {
            System.out.println("Die LKWs sind nicht von der gleichen Marke.");
        } else {
            System.out.println("Die LKWs sind von der gleichen Marke.");
        }
        /*
         * int a = 5, b = 5, c = 2; double testr = 0; double testrr = 0; testrr =
         * (Double.valueOf(5) * Double.valueOf(5)) / Double.valueOf(2); testr =
         * (Double.valueOf(a) * Double.valueOf(b)) / Double.valueOf(c);
         * System.out.println(testr); System.out.println(testrr); bla = (double) 5;
         */
        switch (pkw1) {
            case "Audi":
                rabatt = (5d * 5d) / 2d; 
                if (rabatt > 15.00) {
                    rabatt = 15.00;                    
                }
                preis = preis - (preis/100 * rabatt);
                System.out.println("Der Preis des ersten PKWs ist: " + preis);
                break;

            case "Mercedes":
                rabatt = (5d + 10d)*5d / 2d;
                if (rabatt > 15.00) {
                    rabatt = 15.00;                    
                }
                preis = preis - (preis/100 * rabatt);
                System.out.println("Der Preis des ersten PKWs ist: " + preis);
                break;

            case "Volvo", "VW":
                if (preis % 2 == 0) {
                    preis = 1500d;
                }else{
                    preis = 1700d;
                }
                System.out.println("Der Preis des ersten PKWs ist: " + preis);
                break;
                
            default:
                System.out.println("Der Preis konnte nicht berechnet werden, da die Marke des Fahrzeugs nicht erkennbar ist.");
                break;

        }

    }

}
