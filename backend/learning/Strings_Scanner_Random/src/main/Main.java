package main;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
    
        Scanner userInput = new Scanner(System.in);

        System.out.println("Bitte Name eingeben:");
        String name = userInput.nextLine();
    /*    System.out.println("Bitte Alter eingeben:");
        int age = userInput.nextInt();
        System.out.println("Bitte Gehalt eingeben:");
        double salary = userInput.nextDouble();

        System.out.println("Name: " + name + " Alter: " + age + " Gehalt: " + salary);
        */

        System.out.println("Länge der Strings: " + name.length());
        int esImString = 0;
        if (esImString <= name.indexOf("e")) {
            System.out.println("Position des ersten Es begonnen bei 0: " + name.indexOf("e"));
        }else if (esImString <= name.indexOf("E")) {
            System.out.println("Position des ersten Es begonnen bei 0: " + name.indexOf("E"));
        }else{
            System.out.println("Es sind keine Es im String");
        }        

        int count = 0;
        for (int i = 0; i < name.length(); i++) {
            if (name.charAt(i) == 'o') {
                count++;
            }
        }
        System.out.println("Es sind " + count + " O's im String");

        Random rng = new Random();
        int rngZahlRandom = rng.nextInt(10);
        System.out.println("Zufallszahl mit Random: " + rngZahlRandom);

        int rngZahlMath = (int) (Math.random() * (10 - 2 +1) + 2);
        System.out.println("Zufallszahl mit Math: " + rngZahlMath);

        if (name.length() >= rngZahlMath) {
            System.out.println(name.substring(0, name.length() - rngZahlMath));
        }else{
            System.out.println("Die Randomzahl ist größer als der String");
        }
        
        String letterOutput = "";
        for (int i = 0; i < name.length() ; i++) {
            int randomChar = (int) (Math.random() * 52);
            char base = (randomChar < 26) ? 'A' : 'a';
            char letter = (char) (base + randomChar % 26);
            letterOutput += letter;
        }
        System.out.println(letterOutput);  
        //name = letterOutput;

        if (letterOutput.equals(name)) {
            System.out.println(letterOutput); 
            System.out.println("Juhu!!! Eingabe und Random sind gleich.");
            System.out.println(letterOutput.equals(name));
        }else{
            System.out.println(name + letterOutput);
            System.out.println("Mist!!! Eingabe und Random sind nicht gleich");
            System.out.println(letterOutput.equals(name));
        }

        if (name.startsWith(" ")) {
            System.out.println(name.toLowerCase());
        }else{
            System.out.println(name.toUpperCase());
        }
        
        System.out.println(name.replace(" ", "  "));
        

        System.out.println("Bitte das Heutige Datum eingeben:");
        String datumInput = userInput.nextLine();
        
        userInput.close();
        
        int leerStelle = datumInput.indexOf(" ");
        //System.out.println(leerStelle);

        //Date date =  new Date();
        LocalDate date = LocalDate.now();

        if (leerStelle == 2) {
            //Date formatiert = new SimpleDateFormat("dd/MM/yyyy").parse(datumInput);
            SimpleDateFormat formatiert = new SimpleDateFormat("dd-MM-yyyy");
            datumInput = formatiert.format(datumInput);
         /*   if (formatiert == date ) {
                
            }*/
            System.out.println("Das Heutige Datum ist: " + formatiert.format(datumInput));
        }else if (leerStelle == 4) {
            SimpleDateFormat formatiert = new SimpleDateFormat("yyyy-MM-dd");
            System.out.println("Das Heutige Datum ist: " + formatiert.format(date));
        }else{
            System.out.println("Fehler!");
        }
    }
    
}


