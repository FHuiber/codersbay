package at.aimenflo.impfservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import at.aimenflo.impfservice.entity.Buchung;

public interface BuchungRepository extends JpaRepository<Buchung, Integer> {
    List<Buchung> findByFrei(Boolean frei);
}
