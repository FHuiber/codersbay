package at.aimenflo.impfservice.entity;

public enum GeschlechtEnum {
    M, W, D;
}