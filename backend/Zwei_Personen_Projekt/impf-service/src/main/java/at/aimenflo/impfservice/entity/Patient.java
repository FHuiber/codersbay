package at.aimenflo.impfservice.entity;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.Date;
import java.util.List;

@Entity
@Table(name = "patient")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "svnr")
public class Patient extends Person {

    @Id
    @Column(name="svnr", nullable=false)
    private String svnr;
    
    @Column(nullable = false)
    private String mobilnr, strasse, hausnr, stiege, tuer, plz, ort;

    @Column(name = "geschlecht", nullable = false)
    @Enumerated(EnumType.STRING)
    private GeschlechtEnum geschlecht;

    @OneToOne(mappedBy = "patient", cascade = CascadeType.ALL)
    private Infokanal infokanal;

    @OneToMany(mappedBy = "patient")
    private List<Buchung> buchung;

//--------------------------------CONSTRUCTOR START--------------------------------
    public Patient() {
        super();
    }

    public Patient(String vorname, String nachname, String email, String festnetznr, Date geburtsdatum, String svnr,
            String mobilnr, String strasse, String hausnr, String stiege, String tuer, String plz, String ort,
            GeschlechtEnum geschlecht) {
        super(vorname, nachname, email, festnetznr, geburtsdatum);
        this.svnr = svnr;
        this.mobilnr = mobilnr;
        this.strasse = strasse;
        this.hausnr = hausnr;
        this.stiege = stiege;
        this.tuer = tuer;
        this.plz = plz;
        this.ort = ort;
        this.geschlecht = geschlecht;
    }
//--------------------------------CONSTRUCTOR END--------------------------------

//--------------------------------GET/SET START--------------------------------
    public String getSvnr() {
        return svnr;
    }

    public void setSvnr(String svnr) {
        this.svnr = svnr;
    }

    public String getMobilnr() {
        return mobilnr;
    }

    public void setMobilnr(String mobilnr) {
        this.mobilnr = mobilnr;
    }

    public String getStrasse() {
        return strasse;
    }

    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    public String getHausnr() {
        return hausnr;
    }

    public void setHausnr(String hausnr) {
        this.hausnr = hausnr;
    }

    public String getStiege() {
        return stiege;
    }

    public void setStiege(String stiege) {
        this.stiege = stiege;
    }

    public String getTuer() {
        return tuer;
    }

    public void setTuer(String tuer) {
        this.tuer = tuer;
    }

    public String getPlz() {
        return plz;
    }

    public void setPlz(String plz) {
        this.plz = plz;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }
    
    public GeschlechtEnum getGeschlecht() {
        return geschlecht;
    }

    public void setGeschlecht(GeschlechtEnum geschlecht) {
        this.geschlecht = geschlecht;
    }

    public List<Buchung> getBuchung() {
        return buchung;
    }

    public void setBuchung(List<Buchung> buchung) {
        this.buchung = buchung;
    }

    public Infokanal getInfokanal(){
        return infokanal;
    }

    public void setInfokanal(Infokanal infokanal){
        this.infokanal = infokanal;
    }
//--------------------------------GET/SET END--------------------------------

//--------------------------------ABSTRACT METHOD CALL START--------------------------------
    @Override
    public void test() {
        
    }   
//--------------------------------ABSTRACT METHOD CALL END--------------------------------    
}