package at.aimenflo.impfservice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "infokanal")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id_infokanal")
public class Infokanal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_infokanal", nullable=false)
    private Integer id_infokanal;
    @OneToOne()
    @JoinColumn(name = "svnr", referencedColumnName = "svnr")
    private Patient patient;

    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean mail, sms, tel;

//--------------------------------CONSTRUCTOR START--------------------------------
    public Infokanal(){

    }

    public Infokanal(Integer id_infokanal, boolean mail, boolean sms, boolean tel) {
        this.id_infokanal = id_infokanal;
        this.mail = mail;
        this.sms = sms;
        this.tel = tel;
    }
//--------------------------------CONSTRUCTOR END--------------------------------

//--------------------------------GET/SET START--------------------------------
    public Integer getId_infokanal() {
        return id_infokanal;
    }

    public void setId_infokanal(Integer id_infokanal) {
        this.id_infokanal = id_infokanal;
    }

    public boolean getMail() {
        return mail;
    }

    public void setMail(boolean mail) {
        this.mail = mail;
    }

    public boolean getSms() {
        return sms;
    }

    public void setSms(boolean sms) {
        this.sms = sms;
    }

    public boolean getTel() {
        return tel;
    }

    public void setTel(boolean tel) {
        this.tel = tel;
    }

    public Patient getPatient(){
        return patient;
    }

    public void setPatient(Patient patient){
        this.patient = patient;
    }
//--------------------------------GET/SET END--------------------------------
}