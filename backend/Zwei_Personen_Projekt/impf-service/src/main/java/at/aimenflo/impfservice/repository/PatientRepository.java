package at.aimenflo.impfservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import at.aimenflo.impfservice.entity.Patient;

public interface PatientRepository extends JpaRepository<Patient, String> {
    List<Patient> findByNachnameAndVorname(String nachname, String vorname);
}
