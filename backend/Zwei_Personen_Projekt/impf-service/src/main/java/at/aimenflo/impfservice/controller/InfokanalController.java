package at.aimenflo.impfservice.controller;


import at.aimenflo.impfservice.entity.Infokanal;
import at.aimenflo.impfservice.entity.Patient;
import at.aimenflo.impfservice.repository.InfokanalRepository;
import at.aimenflo.impfservice.repository.PatientRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class InfokanalController {

    @Autowired
    InfokanalRepository infokanalRepository;

    @Autowired
    PatientRepository patientRepository;

//--------------------------------GET ALL INFOKANAL FROM DB START--------------------------------
    @RequestMapping(
        method = RequestMethod.GET,
        path = "/infokanal",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
   public List<Infokanal> getInfokanal() {
        List<Infokanal> infokanalList = infokanalRepository.findAll();
        return  infokanalList;
    }
//--------------------------------GET ALL INFOKANAL FROM DB END--------------------------------

//--------------------------------INSERT/UPDATE INFOKANAL FOR PATIENT INTO DB START--------------------------------
    @RequestMapping(
        method = RequestMethod.POST,
        path = "/infokanal/{svnr}",
        produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public Infokanal setInfokanal(@PathVariable String svnr, @RequestBody Infokanal infokanal){
        Optional<Patient> findPatient = patientRepository.findById(svnr);
        if (findPatient.isEmpty()) {
            throw new RuntimeException("Person nicht in der Datenbank " + svnr);
        }
        Patient patient = findPatient.get();
        if (patient.getInfokanal() != null) {
            //-----Update Infokanal da schon vorhanden-----
            Infokanal infokanaldb = patient.getInfokanal();
            infokanaldb.setMail(infokanal.getMail());
            infokanaldb.setSms(infokanal.getSms());
            infokanaldb.setTel(infokanal.getTel());
            infokanal = infokanalRepository.save(infokanaldb);
        } else{
            //-----Infokanal anlegen da noch nicht vorhanden-----
            infokanal.setPatient(patient);
            infokanal = infokanalRepository.save(infokanal);
        }
        return infokanal;
    }
//--------------------------------INSERT/UPDATE INFOKANAL FOR PATIENT INTO DB END--------------------------------
}