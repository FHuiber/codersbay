package at.aimenflo.impfservice.entity;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name ="buchung")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id_buchung")
public class Buchung {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_buchung;

    @ManyToOne()
    @JoinColumn(name = "svnr", referencedColumnName = "svnr")
    private Patient patient;

    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean frei;

    @Column(nullable = false)
    private String ort;

    @Column(name = "termindatum", nullable = false)
    private LocalDateTime terminDatum;

   @OneToOne(mappedBy = "buchung")
   private Impfstoff impfstoff;

//--------------------------------CONSTRUCTOR START--------------------------------
    public Buchung() {

    }

    public Buchung(Integer id_buchung, Boolean frei, String ort, LocalDateTime terminDatum) {
        this.id_buchung = id_buchung;
        this.frei = frei;
        this.ort = ort;
        this.terminDatum = terminDatum;
    }
//--------------------------------CONSTRUCTOR END--------------------------------

//--------------------------------GET/SET START--------------------------------
    public Integer getId_buchung() {
        return id_buchung;
    }

    public void setId_buchung(Integer id_buchung) {
        this.id_buchung = id_buchung;
    }

    public Boolean getFrei() {
        return frei;
    }

    public void setFrei(Boolean frei) {
        this.frei = frei;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public LocalDateTime getTerminDatum() {
        return terminDatum;
    }

    public void setTerminDatum(LocalDateTime terminDatum) {
        this.terminDatum = terminDatum;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Impfstoff getImpfstoff(){
        return impfstoff;
    }

    public void setImpfstoff(Impfstoff impfstoff){
        this.impfstoff = impfstoff;
    }
//--------------------------------GET/SET END--------------------------------
}