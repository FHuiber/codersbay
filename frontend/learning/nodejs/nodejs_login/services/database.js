const mysql = require('mysql2/promise');
const bcrypt = require('bcryptjs');

let connection;
mysql.createConnection({
    host:  'localhost',
    user: 'root',
    password: '',
    database: 'nodejs'
}).then(con => {
    connection = con;
    console.log('database connected');
}).catch(console.error);

async function createUser({name, email, password}) {
    const passwordHash = await bcrypt.hash(password, 10);
    await connection.execute('INSERT INTO users (name, email, password) VALUES (?, ?, ?)', [name, email, passwordHash]);
}

async function findUserByCredentials({email, password}) {
    const [result] = await connection.execute('SELECT * FROM users WHERE email = ?', [email]);
    if (!result.length) {
        throw new Error('No user with given email address found');
    }
    const user = result[0];
    const doPasswordMatch = await bcrypt.compare(password, user.password);
    if (!doPasswordMatch) {
        throw new Error('Password do not match');
    }
    delete user.password;
    return user;
}

module.exports = {
    createUser,
    findUserByCredentials
}