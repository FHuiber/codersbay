const express = require('express');
const cors = require('cors');
const passport = require('passport');
const UserRouter = require('./router/userRouter.js');
const PublicRouter = require('./router/publicRouter.js');
const LocalStrategy = require('./strategies/localStrategy.js');
const JwtStrategy = require('./strategies/jwtStrategy.js');

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());
app.use(passport.initialize());

passport.use('local', LocalStrategy);
passport.use('jwt', JwtStrategy);

app.use(express.static('public'));

app.use('/api/user', UserRouter);
app.use('/api', PublicRouter);

app.listen(3000, err => {
    if(err) {
        console.error(err);
        return;
    }
    console.log('Webserver running on port 3000');
});


//Old ohne Strategy
/*
const express = require('express');
const port = 3000;
const db = require('./services/database.js');


const app = express();

app.use(express.static('public'));
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.post('/register' , async (req , res)=>{
    if (!req.body.name || !req.body.email || !req.body.password) {
        res.status(400).send('Name, email address or password is missing');
        return;
    }
    try{
       await db.createUser(req.body);
       res.status(201).send('User created');
    }catch (e){
       res.status(500).send(e);
    }
});

app.post('/login', async (req , res)=>{
    if (!req.body.email || !req.body.password) {
        res.status(400).send('Email address or password is missing');
        return;
    }
   try {
       const user = await db.findUserByCredentials(req.body);
       res.json(user);
    }catch (e) {
       res.status(401).send('Wrong credentials');
    }
})


app.listen(port, ()=>{
    console.log(`############### App listening at http://localhost:${port} ###############`);
});
*/