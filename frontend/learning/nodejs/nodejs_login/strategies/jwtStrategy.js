const JwtStrategy = require('passport-jwt').Strategy;
const JwtExtractor = require('passport-jwt').ExtractJwt;
const JwtService = require('../services/jwtService.js');

const strategyOptions = {
    jwtFromRequest: JwtExtractor.fromAuthHeaderAsBearerToken(),
    secretOrKey: JwtService.JWT_SECRET
};
const strategy = new JwtStrategy(strategyOptions, (jwt, done) => {
    const user = {
        userId: jwt.sub,
        name: jwt.name,
        email: jwt.email
    };
    done(null, user);
});

module.exports = strategy;