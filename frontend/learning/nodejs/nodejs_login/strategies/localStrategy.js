const LocalStrategy = require('passport-local').Strategy;
const db = require('../services/database.js');

const strategyOptions = {usernameField: 'email'};
const strategy = new LocalStrategy(strategyOptions,  async (email, password, done) => {
    try {
        const user = await db.findUserByCredentials({email, password});
        done(null, user);
    } catch (err) {
        done(err);
    }
});

module.exports = strategy;
