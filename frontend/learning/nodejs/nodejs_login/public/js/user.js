window.addEventListener('load', async () => {
    const jwt = sessionStorage.getItem('jwt');
    if(!jwt) {
        location.href = 'index.html';
        return;
    }
    const userResponse = await fetch('/api/user', {
       headers: {
           Authorization: 'Bearer ' + jwt
       }
    });
    if(userResponse.status !== 200) {
        location.href = 'index.html';
        return;
    }
    const user = await userResponse.json();
    console.log(user);
    document.getElementById('title').textContent = 'Hallo ' + user.name;
});

function logout() {
    sessionStorage.clear();
    location.href = "index.html";
}

