const loginForm = document.getElementById('login');
const errAlert = document.getElementById('invalidCredentialsAlert');

loginForm.addEventListener('submit', async e => {
    loginForm.classList.add('was-validated');

    e.preventDefault();
    e.stopPropagation();

    if (!loginForm.checkValidity()) {
        return;
    }
    const email = document.getElementById('emailInput').value;
    const password = document.getElementById('passwordInput').value;

    const loginResponse = await fetch('/api/login', {
        method: 'POST',
        body: JSON.stringify({email, password}),
        headers: {
            'Content-Type': 'application/json'
        }
    });
    if(loginResponse.status !== 200) {
        errAlert.style.display = 'block';
        return;
    }
    const jwt = await loginResponse.text();
    sessionStorage.setItem('jwt', jwt);
    location.href = "user.html";
});
