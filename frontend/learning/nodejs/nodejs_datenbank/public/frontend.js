/*
async function getCountries(){
    const countryResponse = await fetch("http://localhost:3000/countries");
    console.log(countryResponse);

    const countries = await countryResponse.json();
    console.log(countries);
}
*/

const countryTable = document.getElementById('countryTableBody');

const refreshBtn = document.getElementById('refresh');
refreshBtn.addEventListener('click', loadCountries);

async function loadCountries() {
    const countryResponse = await fetch('http://localhost:3000/countries');
    const countries = await countryResponse.json();

    countryTable.innerHTML = '';
    for (const country of countries) {
        appendCountryToTable(country);
    }
}

function appendCountryToTable(country) {
    const tr = document.createElement('tr');
    for(let key of ['id', 'name', 'continent', 'area', 'population', 'year_census', 'uno']) {
        const td = document.createElement('td');
        td.textContent = country[key];
        tr.appendChild(td);
    }
    countryTable.appendChild(tr);
}
