const mysql = require('mysql2/promise');

let connection;
mysql.createConnection({
    host: 'localhost',
    user: 'root',
    database: 'uno',
    password: ''
}).then(con => {
    connection = con;
    console.log('Database connected');
}).catch(err => console.error(err));

async function getAllCountries(){
    const [result, info] = await connection.execute('SELECT * FROM countries;');
    return result;
}

async function getCountriesByPopulation(min, max){
    const [result] = await connection.execute('SELECT * FROM countries WHERE population BETWEEN ? AND ?;', [min, max]);
    return result;
}

async function getCountries(){
    const countryResponse = await fetch("http://localhost:3000/countries");
    console.log(countryResponse);

    const countries = await countryResponse.json();
    console.log(countries);
}

module.exports = {
    getAllCountries,
    getCountriesByPopulation
}