const express = require('express');
const app = express();
const port = 3000;
const db = require('./database.js');

const path = require('path');

app.use(express.static('public'));

app.get('/', (req, res)=>{
    //res.send('Hello World!');
    res.sendFile(path.join(__dirname, '/index.html'));
});

app.post('/user', (req, res)=>{
    res.send('Got a Post request');
});

app.get('/download', (req, res)=>{
    res.download(path.join(__dirname, '/Tagesplan Jaba Grundkurs.pdf'), 'Tagesplan Java Grundkurs.pdf');
});

app.get('/countries', async (req, res) => {
    try{
        const countries = await db.getAllCountries();
        res.json(countries);
    }catch(err){
        res.status(500).send(err);
    }
});

app.get('/countries/:min/:max', async (req, res) => {
    try{
        const countries = await db.getCountriesByPopulation(req.params.min, req.params.max);
        res.json(countries);
    }catch(err){
        res.status(500).send(err);
    }
});

//Testen
/*
app.get('/process_get', function (req, res) {
    // Prepare output in JSON format
    response = {
        first_name: req.query.first_name
        , lastname: req.query.last_name
        , password: req.query.password
        , mobile: req.query.user_mobile
        , address: req.query.user_address
    };
    query = db.query("insert query", [params]
        , (err, result) => {
            res.end(JSON.stringify(response));
        })

})
*/

app.listen(port, ()=>{
    console.log(`Example app listening at http://localhost:${port}`);
});
