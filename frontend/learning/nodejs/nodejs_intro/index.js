const http = require('http');
const url = require('url');

//const hostname = '127.0.0.1';
const hostname = '10.217.50.113';
const port = 3000;

/*
//normal function
function testFunction(parameter1, parameter2) {
    console.log(parameter1)
}
*/

/*
//Arrow function, hat keinen namen, kann nur dort aufgerufen werden wo sie definiert wird
(parameter1, parameter2) => {
    console.log(parameter1)
}
*/

const server = http.createServer((req, res)=>{

    //url.resolve('', '/two');

    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/html');
    //res.end('<h1>Flo Hello World Flo</h1>');´
    const fs = require('fs');
    fs.readFile('index.html', (err, data)=>{
        res.end(data);
    });
})

server.listen(port, hostname, ()=>{
    console.log('Server running at http:// ' + hostname + ':' + port);
})