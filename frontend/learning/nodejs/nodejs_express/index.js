const express = require('express');
const app = express();
const port = 3000;

const path = require('path');

app.use(express.static('public'));

app.get('/', (req, res)=>{
    //res.send('Hello World!');
    res.sendFile(path.join(__dirname, '/index.html'));
});

app.post('/user', (req, res)=>{
    res.send('Got a Post request');
});

app.get('/download', (req, res)=>{
    res.download(path.join(__dirname, '/Tagesplan Jaba Grundkurs.pdf'), 'Tagesplan Java Grundkurs.pdf');
});

app.listen(port, ()=>{
    console.log(`Example app listening at http://localhost:${port}`);
});