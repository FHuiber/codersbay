const express = require('express'); //npm install -save express
const cors = require('cors'); //npm install -save cors
const passport = require('passport'); //npm install -save passport
const UserRouter = require('./router/userRouter.js');
const PublicRouter = require('./router/publicRouter.js');
const LocalStrategy = require('./strategies/localStrategy.js');
const JwtStrategy = require('./strategies/jwtStrategy.js');
var hbs = require('hbs');

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());
app.use(passport.initialize());
app.set('view engine', 'hbs');

passport.use('local', LocalStrategy);
passport.use('jwt', JwtStrategy);

app.use(express.static('public'));

app.use('/api/user', UserRouter);
app.use('/api', PublicRouter);

app.listen(8080, err => {
    if(err) {
        console.error(err);
        return;
    }
    console.log('Webserver running on port 8080');
});

//https://www.bezkoder.com/node-js-upload-image-mysql/
//https://stackoverflow.com/questions/61721752/how-can-i-conditionally-render-from-2-navbar-choices-using-node-ejs-passport-a