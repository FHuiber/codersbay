CREATE DATABASE HUIBER_SITE;

USE huiber_site;

CREATE TABLE user (
    userid INT(255) PRIMARY KEY AUTO_INCREMENT NOT NULL, 
    name VARCHAR(100) NOT NULL, 
    email VARCHAR(100) NOT NULL,
    passwort VARCHAR(100) NOT NULL,
    img MEDIUMBLOB,
    created DATETIME NOT NULL
);

INSERT INTO user VALUE (NULL, 'Florian', 'flo@rian.at', '1234', NULL, CURRENT_TIMESTAMP);

DROP TABLE user;