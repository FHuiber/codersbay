const mysql = require('mysql2/promise'); //npm install -save mysql2
const bcrypt = require('bcryptjs'); //npm install -save bcryptjs

let connection;
mysql.createConnection({
    host:  'localhost',
    user: 'root',
    password: '',
    database: 'huiber_site'
}).then(con => {
    connection = con;
    console.log('database °huiber_site° connected');
}).catch(console.error);

async function createUser({name, email, password}) {
    const passwordHash = await bcrypt.hash(password, 10);
    const [dbresult] = await connection.query('SELECT email FROM user WHERE email = ?', [email]);
    if (!dbresult.length) {
        console.log(email);
        console.log(dbresult);
        await connection.execute('INSERT INTO user (name, email, password) VALUES (?, ?, ?)', [name, email, passwordHash]);
    } else {
        throw new Error('Email bereits vorhanden');
    }
    const user = dbresult[0];
    return user;
}

async function findUserByCredentials({email, password}) {
    const [result] = await connection.execute('SELECT * FROM user WHERE email = ?', [email]);
    if (!result.length) {
        throw new Error('No user with given email address found');
    }
    const user = result[0];
    const doPasswordMatch = await bcrypt.compare(password, user.password);
    if (!doPasswordMatch) {
        throw new Error('Password do not match');
    }
    delete user.password;
    return user;
}

module.exports = {
    createUser,
    findUserByCredentials
}