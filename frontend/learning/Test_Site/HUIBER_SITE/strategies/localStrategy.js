const LocalStrategy = require('passport-local').Strategy; //npm install -save passport-local
const db = require('../services/databaseService.js');

const strategyOptions = {usernameField: 'email'};
const strategy = new LocalStrategy(strategyOptions,  async (email, password, done) => {
    try {
        const user = await db.findUserByCredentials({email, password});
        done(null, user);
    } catch (err) {
        done(err);
    }
});

module.exports = strategy;
