const registerForm = document.getElementById('register');
const errAlert = document.getElementById('invalidCredentialsAlert');

registerForm.addEventListener('submit', async e => {
    registerForm.classList.add('was-validated');

    e.preventDefault();
    e.stopPropagation();

    if (!registerForm.checkValidity()) {
        return;
    }
    const name = document.getElementById('nameInput').value;
    const email = document.getElementById('mailInput').value;
    const password = document.getElementById('passInput').value;

    const registerResponse = await fetch('/api/register', {
        method: 'POST',
        body: JSON.stringify({name, email, password}),
        headers: {
            'Content-Type': 'application/json'
        }
    });
    if(registerResponse.status !== 201) {
        errAlert.style.display = 'block';
        return;
    }

    const jwt = await registerResponse.text();
    localStorage.setItem('jwt', jwt);
    location.href = "login.html";
});
