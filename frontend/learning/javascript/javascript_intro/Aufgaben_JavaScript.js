var count = 0;

function handleClick() {
    count++;

    document.getElementById("clickCounter");
    clickCounter.innerHTML = count;
}

function valuecal(result){
    calculatorForm.evalresult.value = calculatorForm.evalresult.value + result;
}

function calc(sym) {
    var buttonvalue = sym;
    var result = 0;
    var USERINPUTNUMBERONE = parseInt(document.getElementById("INPUTNUMBERONE").value);
    var USERINPUTNUMBERTWO = parseInt(document.getElementById("INPUTNUMBERTWO").value);


    if (buttonvalue == "+") {
        result = USERINPUTNUMBERONE+USERINPUTNUMBERTWO;
    }else if(buttonvalue == "-"){
        result = USERINPUTNUMBERONE-USERINPUTNUMBERTWO;
    }else if(buttonvalue == "/"){
        result = USERINPUTNUMBERONE/USERINPUTNUMBERTWO;
    }else if(buttonvalue == "*"){
        result = USERINPUTNUMBERONE*USERINPUTNUMBERTWO;
    }
    

    document.getElementById("OUTPUT").innerHTML = result;
    
}

function textadd() {
    var txtinput = document.getElementById("txtinput").value;
    var txtoutput = document.getElementById("txtoutput").textContent;
    var txtaddresult = txtoutput+txtinput;

    document.getElementById("txtoutput").innerHTML = txtaddresult;
    document.getElementById("txtinput").value = "";
}

function txtclearoutput() {
    document.getElementById("txtoutput").textContent = "Test";
}

function txtdeletelast() {
    var newtext = document.getElementById("txtoutput").textContent;
    newtext = newtext.substr(0, newtext.length - 1);

    document.getElementById("txtoutput").textContent = newtext;
}

function mixColors() {
    let red = document.getElementById("red").checked;
    let green = document.getElementById("green").checked;
    let blue = document.getElementById("blue").checked;
    let mixed;

    if (red) {
        if (green) {
            if (blue) {
                mixed = "Weiß";
            }else{
                mixed = "Gelb";
            }
        }else if (blue) {
            mixed = "Magenta";
        }else{
            mixed = "Rot";
        }
    }else if (green) {
        if (blue) {
            mixed = "Cyan";
        }else{
            mixed = "Grün";
        }
    }else if (blue) {
        mixed = "Blau";
    }else{
        mixed = "Schwarz";
    }
    document.getElementById("mixedColor").innerHTML = mixed;
    /*const checkboxes = document.querySelectorAll('input[name="color"]:checked');

    let colors = [];
    checkboxes.forEach((checkbox) => {
        colors.push(checkbox.value);
    });*/
    
    
}