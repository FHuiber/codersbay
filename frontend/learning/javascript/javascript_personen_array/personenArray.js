class Person {
    constructor(firstName, lastName, birthYear){
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthYear = birthYear;
    }

    get getfirstName(){
        return this.firstName;
    }
    set setfirstName(fn){
        this.firstName = fn;
    }

    get getlastName(){
        return this.lastName;
    }
    set setlastName(ln){
        this.lastName = ln;
    }

    get getbirthYear(){
        return this.birthYear;
    }
    set setbirthYear(by){
        this.birthYear = by;
    }

    getAge(){
        var currentYear = new Date().getFullYear();
        let date = currentYear - this.birthYear;
        return date;
    }

}

var personen = [];

function addPerson() {
    let newPerson = new Person(document.getElementById("firstName").value, document.getElementById("lastName").value, document.getElementById("birthYear").value);

    // mit Set
    //newPerson.setfirstName = "Hansl";

    personen.push(newPerson);

    console.log(personen);
    //console.log(personen[0].getAge());
    
    // clear input fields
    document.getElementById("firstName").value = "";
    document.getElementById("lastName").value = "";
    document.getElementById("birthYear").value = "";

    //document.getElementById("listBody").innerHTML = personen;

    updateTable();
}

function removeAll() {
    listBody.innerHTML = ""
    //console.log(personen);
    personen.length = 0;

    //oder
    // personen = [];
    //updateTable();
}

function removeLast(){
    personen.pop();

    console.log(personen);
    updateTable();
}

function sortByLastName(){
    personen.sort(function(a, b){
        if (a.getlastName < b.getlastName) {
            return -1;
        }else if (a.getlastName > b.getlastName) {
            return 1;
        }else{
            return 0;
        }
    });
    updateTable();
}

function sortByAge() {
    personen.sort(function (a, b) {
        return a.getAge() - b.getAge();
    });
    updateTable();
}

function updateTable() {
    var html = "";
    for (let i = 0; i < personen.length; i++) {
        html += "<tr><td>" + personen[i].getfirstName + 
                "</td><td>" + personen[i].getlastName +
                "</td><td>" + personen[i].getAge() +
                "</td></tr>"
    }
    var listBody = document.getElementById('listBody');
    listBody.innerHTML = html;
}