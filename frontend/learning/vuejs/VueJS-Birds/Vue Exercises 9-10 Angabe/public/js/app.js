//const { default: Axios } = require("axios");

let vm = new Vue({
  el: '#app',
  data: {
    startH1: 'Endangered Birds',
    birds: []
  },
  methods: {
    async getBirds(){
      const response = await axios.get('http://localhost:3000/birds');
      this.birds = response.data;
      //axios.post('http://localhost:3000/birds')
    },
    getBirdAuthors(bird){
      let output = [];
      for(let i in bird.attributes) {
        output.push(bird.attributes[i].author);
      }
      return output.join(', ');
    }
  }
});
/*
Vue.component('birds', {
  template: `
  <div class="birds">
    <h1>Endangered Birds</h1>
    <button>Get Birds!</button>
  </div>
  `
})
*/